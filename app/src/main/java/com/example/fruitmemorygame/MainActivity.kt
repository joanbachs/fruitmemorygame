package com.example.fruitmemorygame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val easyButton = findViewById<Button>(R.id.easyMode)
        val hardButton = findViewById<Button>(R.id.hardMode)

        easyButton.setOnClickListener {
            val intentPlayEasy = Intent(this, GameActivity::class.java)
            startActivity(intentPlayEasy)
        }
        hardButton.setOnClickListener {
            val intentPlayHard = Intent(this, HardGameActivity::class.java)
            startActivity(intentPlayHard)
        }

        val botonHelp = findViewById<Button>(R.id.helpButton)

        botonHelp.setOnClickListener{
            val intentHelp = Intent(this, HelpActivity::class.java)
            startActivity(intentHelp)
        }
    }
}