package com.example.fruitmemorygame

data class Carta(val id: Int, val resId: Int, var girada: Boolean = false)